MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --no-builtin-variables

CXX          = g++
CXXFLAGS     = --std=c++17 -Wall -Wextra -pedantic -static
CXXFLAGS	 += -Ofast -march=native -DRTE_ON_FAILURE

DEPFLAGS     = -MT $@ -MMD -MP -MF ./build/$*.d

MAIN_FILES   = $(shell grep -lr 'int main' './src/' | grep 'cpp$$')
OTHER_FILES  = $(shell grep -Lr 'int main' './src/' | grep 'cpp$$')

SOURCE_FILES = $(OTHER_FILES) $(MAIN_FILES)
EXE_FILES    = $(MAIN_FILES:./src/%.cpp=./exe/%)
OBJECT_FILES = $(OTHER_FILES:./src/%.cpp=./build/%.o)
HEADER_FILES = $(OTHER_FILES:.cpp=.hpp)
DEPENDENCIES = $(SOURCE_FILES:./src/%.cpp=./build/%.d)

compile: $(EXE_FILES)

./build/%.o: ./src/%.cpp ./build/%.d
	@mkdir -p `dirname $@`
	$(CXX) $(CXXFLAGS) $(DEPFLAGS) -c -o $@ $<

$(EXE_FILES): ./exe/%: ./src/%.cpp $(OBJECT_FILES)
	@mkdir -p `dirname $@`
	$(CXX) $(CXXFLAGS) -o $@ $< $(OBJECT_FILES)

.PHONY: clean
clean:
	rm -rf build/ exe/

$(DEPENDENCIES):
include $(wildcard $(DEPENDENCIES))
