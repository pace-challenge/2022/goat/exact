# PACE 2022

This is the solution of GOAT team for the exact track of the PACE 2022 challenge.
The source code is open source under the MIT License.

## Brief description

Branch and reduce solver branching on shortest cycles.

## Instalation

 * Modern version of GCC with C++17 support is required. 

 * Run `make` to compile. (Use `make -j <number_of_cores>` to compile the solver with more cores.)

 * The resulting executable is `./exe/cyclebranch_solver`. 


 * To remove the executables and all build files run `make clean`.

## Used external source code

The only external source code is [robin_hood unordered map & set](https://github.com/martinus/robin-hood-hashing), which is already included in the solver. 


## A link to the solver description in PDF

The solver description is available in the `paper.pdf` file.

