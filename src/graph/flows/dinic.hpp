#pragma once

#include <queue>
#include "../directed_graph.hpp"
#include "flow_edge.hpp"


struct dinic
{
	dinic(size_t n, directed_graph::vertex_t s, directed_graph::vertex_t t);
	void add_edge(directed_graph::vertex_t u, directed_graph::vertex_t v, flow_t c = 1);
	bool bfs();
	flow_t dfs(directed_graph::vertex_t u, flow_t flow);
	void solve_flows();
	
	std::vector <flow_edge> edges;
	std::vector <std::vector<size_t>> adj;
	std::vector<int> level;
	std::queue <directed_graph::vertex_t> q;
	std::vector <std::vector<size_t>::iterator> starts;
	directed_graph::vertex_t s, t;
};





