#include "ford_fulkerson.hpp"

using namespace std;

ford_fulkerson::ford_fulkerson(size_t _n, directed_graph::vertex_t _s, directed_graph::vertex_t _t) : adj(_n), visited(_n), n(_n), s(_s), t(_t) {}

void ford_fulkerson::add_edge(directed_graph::vertex_t u, directed_graph::vertex_t v, flow_t c)
{
	edges.emplace_back(v, c);
	edges.emplace_back(u, 0);
	adj[u].push_back(m);
	adj[v].push_back(m + 1);
	m += 2;
}
flow_t ford_fulkerson::dfs(directed_graph::vertex_t u, flow_t flow)
{
	if(u == t)
		return flow;
	visited[u] = true;
	for(size_t edge_id: adj[u])
	{
		auto&[v, c, f] = edges[edge_id];
		
		if(!visited[v])
		{
			if(c > f)
			{
				flow_t forward_flow = dfs(v, min(flow, c - f));
				if(forward_flow > 0)
				{
					edges[edge_id].f += forward_flow;
					edges[edge_id ^ 1].f -= forward_flow;
					return forward_flow;
				}
			}
		}
	}
	return 0;
}
flow_t ford_fulkerson::max_flow()
{
	flow_t max_flow = 0;
	flow_t flow;
	do
	{
		visited.assign(n, false);
		flow = dfs(s, std::numeric_limits<flow_t>::max());
		max_flow += flow;
	} while(flow != 0);
	return max_flow;
}

pair<bool, size_t> ford_fulkerson::max_flow(size_t k)
{
	flow_t max_flow = 0;
	flow_t flow;
	do
	{
		visited.assign(n, false);
		flow = dfs(s, std::numeric_limits<flow_t>::max());
		max_flow += flow;
		if((size_t)max_flow > k)
		{
			return {false, (size_t)max_flow};
		}
	} while(flow != 0);
	return {true, (size_t)max_flow};
}