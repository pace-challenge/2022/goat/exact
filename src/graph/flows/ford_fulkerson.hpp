#pragma once

#include <vector>
#include "flow_edge.hpp"

struct ford_fulkerson
{
	const flow_t INF = std::numeric_limits<flow_t>::max();
	std::vector <flow_edge> edges;
	std::vector <std::vector<size_t>> adj;
	std::vector<bool> visited; //TODO: use custom bitset
	size_t n, m = 0;
	directed_graph::vertex_t s, t;
	ford_fulkerson(size_t _n, directed_graph::vertex_t _s, directed_graph::vertex_t _t);
	void add_edge(directed_graph::vertex_t u, directed_graph::vertex_t v, flow_t c);
	flow_t dfs(directed_graph::vertex_t u, flow_t flow);
	flow_t max_flow();
	std::pair<bool, size_t> max_flow(size_t k);
};




