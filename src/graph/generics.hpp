#include <vector>
#include "../fast_hash_table.hpp"

/*
 * Calculates intersection of a and b
 * Both vectors a and b has to be sorted in some order
 */
template <typename T>
std::vector<T> get_intersection_sorted(std::vector<T> a, std::vector<T> b){
    std::vector<T> result;
    size_t i, j;
    i = j = 0;
    while(i < a.size() && j < b.size()){
        if(a[i] == b[j]){
            result.push_back(a[i]);
            ++i;
            ++j;
        } else if(a[i] < b[j]){
            ++i;
        } else{
            ++j;
        }
    }
    return result;
}

template <typename T>
fast_set<T> get_intersection(const fast_set<T> & A, const  fast_set<T> & B ){
    if(A.size() > B.size()){
        return get_intersection(B, A);
    }
    fast_set<T> res;
    for(const T & a : A){
        if(B.find(a) != B.end()){
            res.insert(a);
        }
    }
    return res;
}


template<typename InputIt>
bool is_subset_of(InputIt abegin, InputIt aend, InputIt bbegin, InputIt bend)
{
	if(std::distance(abegin, aend) > std::distance(bbegin, bend))
	{
		return false;
	}
	else if(abegin == aend)
	{
		return true;
	}
	if(*abegin == *bbegin)
	{
		return is_subset_of(++abegin, aend, ++bbegin, bend);
	}
	return is_subset_of(abegin, aend, ++bbegin, bend);
}
/*!
 * Check if container a is subset of container b (container must be sorted, otherwise undefined behaviour).
 */
template<typename Cont>
bool is_subset_of(const Cont& a, const Cont& b)
{
	return is_subset_of(a.begin(), a.end(), b.begin(), b.end());
}
