#include "../utils.hpp"
#include <iostream>
#include "../generics.hpp"

using std::vector;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;

bool is_clique(const directed_graph &g, const vertex_set_t &in)
{
	size_t cliqueSize = in.size();
	for (const auto &v : in)
	{
		if (g.in_degree(v) + 1 < cliqueSize ||  g.out_degree(v) + 1 < cliqueSize)
			return false;
	}

	for (const auto &v : in)
	{
		for (const auto &u : in)
		{
			if (v == u)
				continue;
			if (!g.contains_edge(v, u))
				return false;
		}
	}

	return true;
}

bool neighbour_to_all(const directed_graph & graph, const vertex_vec_t & vertices, vertex_t v ){
	// probably faster, at least for larger cliques
	if(graph.in_degree(v) < vertices.size() || graph.out_degree(v) < vertices.size()){
		return false;
	}
	for(vertex_t u :vertices){
		if(	!graph.contains_edge(u, v) || !graph.contains_edge(v,u)){
			return false;
		}
	}
	return true;
}

// vertices in current_clique are in ascending order - next vertex must be greater than the last
// we are looking only to neighbours of the smallest_deg_vertex
void find_allkCliques_inner(const directed_graph& graph, size_t k,
						   vertex_vec_t& current_clique, 
						   vertex_t smallest_deg_vertex,
						   vector <vertex_vec_t>& result){
    if(current_clique.size() == k){
		result.push_back(current_clique);
		return;
	}
	for(vertex_t v: graph.successors(smallest_deg_vertex)){
		if(v > current_clique[current_clique.size()-1] && neighbour_to_all(graph, current_clique, v)){
			vertex_t new_smallest_deg = (graph.out_degree(smallest_deg_vertex) < graph.out_degree(v)) ? smallest_deg_vertex: v;
			current_clique.push_back(v);
			find_allkCliques_inner(graph,k, current_clique, new_smallest_deg, result);
			current_clique.pop_back();
		}
	}
}

vector<vertex_vec_t> find_allkCliques(const directed_graph& graph, size_t k){
	vector<vertex_vec_t> result;
	for(vertex_t v : graph.get_vertices()){
		vertex_vec_t accumulator = {v};
		find_allkCliques_inner(graph, k, accumulator, v, result);
	}
	return result;
}

vertex_set_t heuristic_maximal_clique(const directed_graph& graph){
	vertex_set_t intersection; // all vertices that are incident to the clique
	vertex_set_t clique;
	fast_map<vertex_t, vertex_set_t> neighborhoods;
	for(vertex_t v: graph.get_vertices()){
			// precalculating the two way neigborhoods
			vertex_vec_t v_neighborhood_vec = get_two_way_neigborhood(graph, v); 
			vertex_set_t v_neighborhood(v_neighborhood_vec.begin(), v_neighborhood_vec.end());
			v_neighborhood.insert(v);
			neighborhoods[v] = v_neighborhood;
			// first clique is the vertex with highest degree
			if(v_neighborhood.size() > intersection.size()){
				intersection = v_neighborhood;
				clique.clear();
				clique.insert(v);
			}
	}
	// we select greedily next vertex to clique by size of the neigborhood intersection with @intersection
	while(intersection.size() > clique.size()){
		vertex_set_t max_neighborhood;
		vertex_t best_vertex = *graph.get_vertices().begin(); // one vertex is clique
		for(vertex_t v : intersection){
			// in intersection is also the clique
			if(clique.find(v) != clique.end()){
				continue;
			}
			vertex_set_t & v_neighborhood = neighborhoods[v];
			if(v_neighborhood.size() < max_neighborhood.size()){ // minor speedup - skip nonperspective vertex
				continue;
			}
			vertex_set_t intersected_neigborhood = get_intersection(v_neighborhood, intersection);
			if(intersected_neigborhood.size() > max_neighborhood.size()){
				max_neighborhood = intersected_neigborhood;
				best_vertex = v;
			}
			neighborhoods[v] = intersected_neigborhood;
		}
		clique.insert(best_vertex);
		intersection = max_neighborhood;
	}
	return clique;
}



vertex_vec_t heuristic_maximal_clique_2(const directed_graph& graph){
	vector<std::pair<int, vertex_t>> degree_vertex;
	for(vertex_t v : graph.get_vertices()){
		degree_vertex.push_back({graph.in_degree(v) + graph.out_degree(v), v});
	}
	std::sort(degree_vertex.begin(), degree_vertex.end(), std::greater<std::pair<int, vertex_t>>());
	vertex_vec_t clique;
	int i = 0;
	while(i < (int)degree_vertex.size()){
		vertex_t v = degree_vertex[i].second;
		if(neighbour_to_all(graph, clique, v)){
			clique.push_back(v);
		} else if(graph.in_degree(v) + graph.out_degree(v) < 2*clique.size()){
			break;
		}
		++i;
	}
	return clique;
}