#include <stack>
#include "../utils.hpp"

using namespace std;
const int undefined = -1;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;

void dfs_r(vertex_t v, const directed_graph& g, fast_map<vertex_t, bool>& seen, stack<vertex_t>& z)
{
	// to preserve the dfs recursive order, we have to validate seen later / not immediately when we see the vertex but when we are about to jumpi into it

	// two types of items - vertex that has to be examined (true) and a note that successors of this vertices are all above this - marks the order (false) 
	stack<pair<bool, vertex_t>> s({{true, v}});
	while(!s.empty()){
		pair<bool, vertex_t> cur = s.top();
		s.pop();
		// the stop item - just mark the order
		if(!cur.first){ // this goes before seen check because we have to do this, and when we do this the verte
			z.push(cur.second);
			continue;
		}
		if(seen[cur.second]){ // the vertex can be in the stack multiple times
			continue;
		}
		seen[cur.second] = true;
		s.push({false, cur.second}); // place the mark for order first
		for(vertex_t p: g.predecessors(cur.second))
		{
			if(!seen[p])
			{
				s.push({true, p});
			}
		}
	}
}

void dfs(vertex_t v, const directed_graph& g, int comp, fast_map<vertex_t, int>& comps)
{
	stack<vertex_t> s ({v});
	comps[v] = comp;
	while(!s.empty()){
		vertex_t cur = s.top();
		s.pop();
		for(vertex_t neigh: g.successors(cur))
		{
			if(comps[neigh] == undefined)
			{
				s.push(neigh);
				comps[neigh] = comp;
			}
		}
	}
}

vector <vertex_vec_t> strongly_connected_components(const directed_graph& g)
{
	fast_map<vertex_t, bool> seen;
	stack<vertex_t> z {};
	for(vertex_t v: g.get_vertices())
	{
		if(!seen[v])
		{
			dfs_r(v, g, seen, z);
		}
	}
	fast_map<vertex_t, int> comps;
	for(vertex_t v: g.get_vertices())
	{
		comps[v] = undefined;
	}
	int comp = 0;
	while(!z.empty())
	{
		vertex_t v = z.top();
		z.pop();
		if(comps[v] == undefined)
		{
			dfs(v, g, comp++, comps);
		}
	}
	vector <vertex_vec_t> res(comp);
	for(const auto&[v, c]: comps)
	{
		res[c].push_back(v);
	}
	return res;
}


vector<pair<vertex_t, vertex_t>> get_non_scc_edges(const directed_graph& graph){
    // color vertices according to component, then edge has to be single colored or we remove it
	vector<vertex_vec_t> sccs = strongly_connected_components(graph);
	if(sccs.size() == 1){
		return {};
	}
    fast_map<vertex_t, int> color;
    int c = 0;
    for(const vertex_vec_t & component : sccs){
        for(vertex_t v : component){
            color[v] = c;
        }
        ++c;
    }
    vector<pair<vertex_t, vertex_t>> non_scc_edges;
    for(vertex_t u : graph.get_vertices()){
        for(vertex_t v : graph.successors(u)){
            if(color[u] != color[v]){
                non_scc_edges.push_back({u,v});
            }
        }
    }
    return non_scc_edges;
}