#include <queue>
#include "../utils.hpp"
#include "../../fast_hash_table.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;

vertex_vec_t topsort(const directed_graph& graph)
{
	vertex_vec_t result;
	fast_map<vertex_t, int> in_degrees;

	queue<vertex_t> q;

	for(vertex_t v: graph.get_vertices())
	{
		in_degrees[v] = graph.in_degree(v);
		if(in_degrees[v] == 0)
		{
			q.push(v);
		}
	}

	while(!q.empty())
	{
		vertex_t v = q.front();
		result.push_back(v);
		for(vertex_t s: graph.successors(v))
		{
			if(--in_degrees[s] == 0)
			{
				q.push(s);
			}
		}
		q.pop();
	}
	return result;
}

//TODO: Naimplementovat tohle ručně jako topsort bez vectoru result
bool is_DAG(const directed_graph& graph)
{
	return topsort(graph).size() == graph.get_vertices_count();
}