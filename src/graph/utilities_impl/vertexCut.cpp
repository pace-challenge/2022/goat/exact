#include "../utils.hpp"
#include <vector>
#include "../../fast_hash_table.hpp"
using namespace std;

using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_t = directed_graph::vertex_t;

void AP_DFS(const directed_graph & graph,
            std::vector<vertex_vec_t> & res,
            int & time,
            fast_map<vertex_t,
            int> &in, fast_map<vertex_t, int> &low,
            vertex_t parent, vertex_t cur)
{
    in[cur] = low[cur] = time++;

    bool isArticulationPoint = false;
    int children = 0;
    //iterates through edges after symmetrization
    for(const auto & neigh : graph.neighbours(cur))
    {
        if(in[neigh] == -1) //visited == false, edge downwards
        {
            children++;
            AP_DFS(graph, res, time, in, low, cur, neigh);
            low[cur] = min(low[cur], low[neigh]);
            if(parent != -1 && low[neigh] >= in[cur]) //subtree edges dont reach above ``cur``
                isArticulationPoint = true;
        }
        else if(neigh != parent) //edge upwards, closes cycle
        {
            low[cur] = min(low[cur], in[neigh]);
        }
    }

    if(parent == -1 && children >= 2)
        isArticulationPoint = true;

    if(isArticulationPoint)
        res.push_back({cur});
}

std::vector<directed_graph::vertex_vec_t> get_articulation_points(const directed_graph & graph)
{
    //swap to hashmap
    fast_map<directed_graph::vertex_t, int> in, low;
    int time = 0;
    for(const auto & v : graph.get_vertices()) in[v]=low[v] = -1;
    vector<vertex_vec_t> ret;
    for(auto start : graph.get_vertices())
    {
        if(in[start] == -1)
            AP_DFS(graph, ret, time, in, low, -1, start);
    }
    return ret;   
}

//------------------------------------------------------------------------------

std::vector<directed_graph::vertex_vec_t> get2Cut(const directed_graph & g)
{
    std::vector<directed_graph::vertex_vec_t> ret;
    auto vec = g.get_vertices_vec();
    for(auto v : vec)
    {
        directed_graph copy = g;
        copy.remove_vertex(v);
        auto articulations = get_articulation_points(copy);
        std::sort(articulations.begin(), articulations.end(), 
            [](const directed_graph::vertex_vec_t & a, const directed_graph::vertex_vec_t & b)
                {return a.front() < b.front();});

        for(auto & vec : articulations)
        {
            vertex_t otherCut = vec.front();
            if(v < otherCut)
                ret.push_back({v, otherCut});  
        }
    }
    return ret;
}

//------------------------------------------------------------------------------

/**
 * helper function to generate all cuts
 */
template<bool weaklyConnected>
void generateVertexCut(const directed_graph & g, const directed_graph::vertex_vec_t & vertices, const int g_components_size, 
                const int cutSize, int prevVertexIdxUsed, directed_graph::vertex_vec_t & cut,
                vector<directed_graph::vertex_vec_t> & result)
{
    if((int)cut.size() == cutSize)
    {
        directed_graph copy = g;
        for(auto v : cut)
            copy.remove_vertex(v);
        
        if(weaklyConnected)
        {
            if((int)weakly_connected_components(copy).size() != g_components_size)
                result.push_back(cut);
        }
        else
        {
            if((int)strongly_connected_components(copy).size() != g_components_size)
                result.push_back(cut);
        }
        return;
    }

    for(int i = prevVertexIdxUsed+1; i < (int)vertices.size(); i++)
    {
        cut.push_back(vertices[i]);
        generateVertexCut<weaklyConnected>(g, vertices, g_components_size, cutSize, i, cut, result);
        cut.pop_back();
    }
}

template<>
vector<directed_graph::vertex_vec_t> getVertexCut<true>(const directed_graph & g, int cutSize)
{
    if(cutSize == 1)
        return get_articulation_points(g);
    if(cutSize == 2)
        return get2Cut(g);
    auto vec = g.get_vertices_vec();
    int components = weakly_connected_components(g).size();
    vector<directed_graph::vertex_vec_t> ret;
    directed_graph::vertex_vec_t build;
    generateVertexCut<true>(g, vec, components, cutSize, -1, build, ret);
    return ret;
}

template<>
vector<directed_graph::vertex_vec_t> getVertexCut<false>(const directed_graph & g, int cutSize)
{
    auto vec = g.get_vertices_vec();
    int components = strongly_connected_components(g).size();
    vector<directed_graph::vertex_vec_t> ret;
    directed_graph::vertex_vec_t build;
    generateVertexCut<false>(g, vec, components, cutSize, -1, build, ret);
    return ret;
}

