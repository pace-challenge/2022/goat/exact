#include "../utils.hpp"

bool incident_with_out_edge(const directed_graph &g, const directed_graph::vertex_t &v)
{
    if (g.out_degree(v) > g.in_degree(v))
        return true;

    for (const auto &neigh : g.successors(v))
        if (!g.contains_edge(neigh, v))
            return true;

    return false;
}

bool incident_with_in_edge(const directed_graph &g, const directed_graph::vertex_t &v)
{
    if (g.in_degree(v) > g.out_degree(v))
        return true;

    for (const auto &neigh : g.predecessors(v))
        if (!g.contains_edge(v, neigh))
            return true;

    return false;
}

bool incident_with_only_twoWay_edge(const directed_graph &g, const directed_graph::vertex_t &v)
{
    if (g.in_degree(v) != g.out_degree(v))
        return false;

    for (const auto &neigh : g.successors(v))
        if (!g.contains_edge(neigh, v))
            return false;

    return true;
}

bool incident_with_twoWay_edge(const directed_graph &g, const directed_graph::vertex_t &v)
{
    if(g.successors(v).empty())
        return false;

    for (const auto &neigh : g.successors(v))
        if (g.contains_edge(neigh, v))
            return true;
    return false;
}

bool hasOnly_twoWay_edges(const directed_graph &g)
{
    for(auto v : g.get_vertices())
        if(!incident_with_only_twoWay_edge(g, v))
            return false;
    return true;
}

int sum_degrees(const directed_graph::vertex_vec_t & vertices, const directed_graph & graph){
    int sum = 0;
    for(const directed_graph::vertex_t & v : vertices){
        sum += graph.in_degree(v);
        sum += graph.out_degree(v);
    }
    return sum;
}

void notInSolution(directed_graph & g, directed_graph::vertex_t v)
{
    auto predecessors = g.predecessors(v);
    auto successors = g.successors(v);
    
    for(auto pred : predecessors)
        for(auto suc : successors)
            g.add_edge(pred, suc);

    g.remove_vertex(v);
}

directed_graph::vertex_vec_t get_two_way_neigborhood(const directed_graph & g, directed_graph::vertex_t v){
    directed_graph::vertex_vec_t result;
    for(directed_graph::vertex_t u : g.successors(v)){
        if(g.contains_edge(u,v)){
            result.push_back(u);
        }
    }
    return result;
}