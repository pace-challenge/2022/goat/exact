#include "../utils.hpp"
#include "../../fast_hash_table.hpp"
#include <stack>

using namespace std;

using vertex_t = directed_graph::vertex_t;

void dfs(const directed_graph & graph, fast_map<directed_graph::vertex_t, bool> & visited,
        vertex_t start, directed_graph::vertex_vec_t & component)
{
    stack<vertex_t> s ({start});
    visited[start] = true;
    component.push_back(start);
    while(!s.empty()){
        vertex_t cur = s.top();
        s.pop();
        for(vertex_t neigh : graph.neighbours(cur))
            if(!visited[neigh]){
                s.push(neigh);
                component.push_back(neigh);
                visited[neigh] = true;
            }
    }
}

std::vector<directed_graph::vertex_vec_t> weakly_connected_components(const directed_graph& graph)
{
    std::vector<directed_graph::vertex_vec_t> ret;
    fast_map<directed_graph::vertex_t, bool> visited;
    for(const auto & v : graph.get_vertices()) visited[v] = false;

    for(const auto & v : graph.get_vertices())
    {
        if(visited[v]) continue;
        ret.push_back({});
        dfs(graph, visited, v, ret.back());
    }

    return ret;
}
