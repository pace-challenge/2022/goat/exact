#pragma once

#include <istream>
#include <vector>

template<typename Graph>
Graph load_graph(std::istream& is);

template<typename Graph>
void output_graph(std::ostream& os,const Graph& graph);


template<typename Graph>
void output_graph_as_json(std::ostream& os,const Graph& graph);