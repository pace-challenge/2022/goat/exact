#pragma once

#include "../graph/directed_graph.hpp"

/*
 * Returns lowerbound for DFVS for @graph using packing cycles and reductions 
 */
int pack_cycles_lb(directed_graph graph);


/*
 * Returns lowerbound for DFVS for @graph using lpvc and @pack_cycles_lb.
 */
int lpvc_plus_pack_cycles_lb(directed_graph graph);


/*
 * Returns lowerbound for DFVS for @graph using packing cycles and reductions, which are applied when no cliques are found
 */
int pack_cliques_lb(directed_graph graph);


/*
 * Returns lowerbound for DFVS for @graph using packing cycles and reductions which are applied immedeatelly
 */
int pack_cliques_lb_slow(directed_graph graph);