#include "chorded_component.hpp"

#include "../graph/utils.hpp"
#include "../graph/generics.hpp"
#include "../fast_hash_table.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;

#include <queue>

vector<pair<vertex_t, vertex_t>> get_noncycle_edges_v_removal(const directed_graph & graph, vertex_t v, 
	                                                            fast_map<vertex_t, vector<pair<vertex_t, vertex_t>>> & cache){
    if(cache.find(v) != cache.end()){
        return cache[v];
    }
    directed_graph copy = graph;
    copy.remove_vertex(v);
    vector<pair<vertex_t, vertex_t>> noncycle_edges = get_non_scc_edges(copy);
    // add edges incident to v, since the are currently in no cycle
    for(vertex_t pred : graph.predecessors(v)){
        noncycle_edges.push_back({pred,v});
    }
    for(vertex_t succ : graph.successors(v)){
        noncycle_edges.push_back({v,succ});
    }
    sort(noncycle_edges.begin(), noncycle_edges.end());
    cache[v] = noncycle_edges;
    return noncycle_edges;
}

bool chorded_component_rule::apply(directed_graph& graph)
{
    bool applied = false;
    queue<directed_graph> q;
    enqueue_oneway_components(q, graph);
    while(!q.empty()){
        directed_graph subgraph = q.front();
        q.pop();
        vector<vertex_vec_t> two_cycles = find_all2Cycles(subgraph);
        directed_graph one_way_subraph = subgraph; 
        remove_2cycles_from_graph(one_way_subraph);
        bool component_changed = false;
	    fast_map<vertex_t, vector<pair<vertex_t, vertex_t>>> cache;
        for(const vertex_vec_t & two_cycle : two_cycles){
            vector<pair<vertex_t, vertex_t>> noncycle_edges0 = get_noncycle_edges_v_removal(one_way_subraph, two_cycle[0], cache);
            vector<pair<vertex_t, vertex_t>> noncycle_edges1 = get_noncycle_edges_v_removal(one_way_subraph, two_cycle[1], cache);
            vector<pair<vertex_t, vertex_t>> intersection = get_intersection_sorted(noncycle_edges0, noncycle_edges1);
            for(auto [u,v] : intersection){
                graph.remove_edge(u,v);
                subgraph.remove_edge(u,v);
                one_way_subraph.remove_edge(u,v);
            }
            if(!intersection.empty()){
                component_changed = true;
                applied = true;
            }
        }
        if(component_changed){
            enqueue_oneway_components(q, subgraph);
        }
    }
    return applied;
}

std::string chorded_component_rule::description() const
{
    return "chorded component";
}
