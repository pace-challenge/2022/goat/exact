#include "clique_chord.hpp"

#include "../graph/utils.hpp"
#include "../graph/generics.hpp"
#include "../fast_hash_table.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;

#include <queue>
#include <iostream>

class vertex_vec_t_hasher {
public:
  std::size_t operator()(std::vector<vertex_t> const& vertices) const {
    std::size_t result = 0;
    for(vertex_t v : vertices) {
      result ^= std::hash<vertex_t>()(v);
    }
    return result;
  }
};

clique_chord_rule::clique_chord_rule(size_t clique_size):clique_size(clique_size){}

vector<pair<vertex_t, vertex_t>> get_noncycle_edges_after_removal(const directed_graph & graph, vertex_vec_t vertices, 
                fast_map_hash<vertex_vec_t, vector<pair<vertex_t, vertex_t>>, vertex_vec_t_hasher> & cache){
    if(cache.find(vertices) != cache.end()){
        return cache[vertices];
    }
    directed_graph copy = graph;
    for(vertex_t v: vertices){
        copy.remove_vertex(v);
    }
    vector<pair<vertex_t, vertex_t>> noncycle_edges = get_non_scc_edges(copy);
    // add edges incident to vertices, since the are currently in no cycle
    for(vertex_t v: vertices){
        for(vertex_t pred : graph.predecessors(v)){
            noncycle_edges.push_back({pred,v});
        }
        for(vertex_t succ : graph.successors(v)){
            noncycle_edges.push_back({v,succ});
        }
    }
    sort(noncycle_edges.begin(), noncycle_edges.end());
    cache[vertices] = noncycle_edges;
    return noncycle_edges;
}

vector<pair<vertex_t, vertex_t>> get_sorted_edges(const directed_graph & graph){
    vector<pair<vertex_t, vertex_t>> result;
    for(vertex_t v : graph.get_vertices()){
        for(vertex_t u : graph.successors(v)){
            result.push_back({v,u});
        }
    }
    sort(result.begin(), result.end());
    return result;
}

bool clique_chord_rule::apply(directed_graph& graph)
{
    bool applied = false;
    queue<directed_graph> q;
    enqueue_oneway_components(q, graph, clique_size + 1);
    while(!q.empty()){
        directed_graph subgraph = q.front();
        q.pop();
        vector<vertex_vec_t> cliques = find_allkCliques(subgraph, clique_size);
        directed_graph one_way_subraph = subgraph; 
        remove_2cycles_from_graph(one_way_subraph);
        bool component_changed = false;
        // clique_size-1 vetices -> edges they remove
	    fast_map_hash<vertex_vec_t, vector<pair<vertex_t, vertex_t>>, vertex_vec_t_hasher> cache;
        for(const vertex_vec_t & clique : cliques){
            // edges to be removed
            vector<pair<vertex_t, vertex_t>> intersection = get_sorted_edges(one_way_subraph);
            for(vertex_t v : clique){
                vertex_vec_t clique_copy = clique; // clique is small, this is fast and keeps the clique sorted
                clique_copy.erase(std::remove(clique_copy.begin(), clique_copy.end(), v), clique_copy.end());
                vector<pair<vertex_t, vertex_t>> noncycle_edges_v = get_noncycle_edges_after_removal(one_way_subraph, clique_copy, cache);
                intersection = get_intersection_sorted(intersection, noncycle_edges_v);
            }
            // removing the chorded edges
            for(auto [u,v] : intersection){
                graph.remove_edge(u,v);
                subgraph.remove_edge(u,v);
                one_way_subraph.remove_edge(u,v);
            }
            if(!intersection.empty()){
                component_changed = true;
                applied = true;
            }
        }
        if(component_changed){
            enqueue_oneway_components(q, subgraph, clique_size + 1);
        }
    }
    return applied;
}

std::string clique_chord_rule::description() const
{
    return "clique chord " + to_string(clique_size);
}
