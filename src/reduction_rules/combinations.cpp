#include<cmath>

#include "../graph/utils.hpp"

#include "combinations.hpp"

#include "edgecontraction.hpp"
#include "uselessvertexdeletion.hpp"
#include "sourcesink.hpp"
#include "remove_self_loops.hpp"
#include "two_way_clique.hpp"
#include "redundant_edges.hpp"
#include "subset_neighbours.hpp"
#include "dome_rule.hpp"
#include "chorded_component.hpp"
#include "vc_style_chord.hpp"
#include "dominated_cycles.hpp"
#include "flower.hpp"
#include "folding.hpp"
#include "struction.hpp"
#include "lpvc_rule.hpp"
#include "neighborhood_is_clique.hpp"
#include "cycle_to_clique.hpp"
#include "subgraph_subset_neigborhood.hpp"

static sourcesink ssr;
static edge_contraction_rule ecr;
static redundant_edges_rule rer;
static useless_vertex_deletion uvd;
static chorded_component_rule ccr;
static vc_style_chord vsc;
static dominated_cycles_rule dcr;
static remove_self_loops rsl;
static two_way_clique twc;
static subset_neighbours_rule snr;
static dome_rule dmr;
static flower flw;
static folding fld;
static struction struc;
static lpvc_rule lpvc;
static neighborhood_is_clique nic;
static cycle_to_clique ctc;
static subgraph_subset_neigborhood ssn;

using vertex_vec_t = directed_graph::vertex_vec_t;

int reduce_all(directed_graph & graph, vertex_vec_t & removed){
    int stack_pointer = removed.size();
    bool flag = true;
    while(flag){
        flag = false;
        flag = flag || ssr.apply(graph);
        flag = flag || ecr.apply(graph);
        flag = flag || rer.apply(graph);
        flag = flag || uvd.apply(graph);
        flag = flag || dmr.apply(graph);
        flag = flag || rsl.apply(graph, removed);
        flag = flag || twc.apply(graph, removed);
        flag = flag || nic.apply(graph);
        flag = flag || snr.apply(graph, removed);
        flag = flag || lpvc.apply(graph, removed);
		
		if(is_DAG(graph)) break;
    }
    return (int)removed.size() - stack_pointer;
}

int reduce_all(directed_graph & graph, directed_graph::vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules)
{
    int oldk = graph.get_vertices_count(); //trivial upperbound
    int k = oldk;
    bool flag = true;
    while(flag){
        flag = false;
        k -= reduce_all(graph, removed);

        flag = flag || fld.apply(graph, removed, k, reverseRules);
		// flag = flag || ctc.apply(graph, removed, k, reverseRules);
		flag = flag || struc.apply(graph, removed, k, reverseRules);

		if(is_DAG(graph)) break;
    }
    return oldk - k;
}


int reduce_expensive(directed_graph & graph, directed_graph::vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules){
	int oldk = graph.get_vertices_count(); //trivial upperbound
	int k = oldk;
    bool flag = true;
    while(flag){
        flag = false;
		k -= reduce_all(graph, removed, reverseRules);
        int stack_pointer = removed.size();
        flag = flag || ssn.apply(graph, removed);
        k -= (int)removed.size() - stack_pointer;

        //flag = flag || ccr.apply(graph); //vsc does a lot more and is only 2 times slower 
        flag = flag || vsc.apply(graph);
        flag = flag || dcr.apply(graph);

		if(is_DAG(graph)) break;
    }
    return oldk - k;
}


int reduce_all(directed_graph & graph, vertex_vec_t & removed, int k, reverse_rules_stack<directed_graph> & reverseRules){
	int oldk = k;
    k -= reduce_all(graph, removed);

    bool flag = true;
    while(flag){
        flag = false;
        flag = flag || fld.apply(graph, removed, k, reverseRules);
        if(k < 0)
            break;

		flag = flag || struc.apply(graph, removed, k, reverseRules);
        if(k < 0)
            break;

        flag = flag || flw.apply(graph, removed, k);
        if(k < 0)
            break;

        k -= reduce_all(graph, removed);
        if(k < 0)
            break;

		if(is_DAG(graph)) break;
    }
    return oldk - k;
}

int dangerous_reduce_all(directed_graph & graph, vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules, int dangerBudget)
{
	int oldk = graph.get_vertices_count(); //trivial upperbound
	int k = oldk;

	bool flag = true;
	bool kIncreased = false;
    while(flag){
        flag = false;
		int startk = k;
		// flag = flag || ctc.apply(graph, removed, k, reverseRules);
		flag = flag || struc.dangerous_apply(graph, removed, k, reverseRules, dangerBudget);
		k -= reduce_all(graph, removed, reverseRules);
		int endk = k;

		if(is_DAG(graph)) break;

		//param increased, proceed with caution
		if(endk >= startk){
			if(kIncreased) //k increased twice in a row, abort
				break;
			else
				kIncreased = true; //in preparation for next time
		}
		else{
			kIncreased = false; //reset counter
		}
    }
    return oldk - k;
}