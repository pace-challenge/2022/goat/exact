#pragma once

#include "parameter_reverse_rule.hpp"
#include "reverse_rule.hpp"
#include "../graph/directed_graph.hpp"
#include "../fast_hash_table.hpp"

class cycle_to_clique : public parameter_reverse_rule<directed_graph>
{
public:
	class cycle_to_clique_reverse : public reverse_rule<directed_graph>
	{
		directed_graph::vertex_set_t newClique;
		directed_graph::vertex_t replacement;

	public:
		cycle_to_clique_reverse(const directed_graph::vertex_vec_t &clique, directed_graph::vertex_t v)
			: newClique(clique.begin(), clique.end()), replacement(v) {}

		virtual bool reverse_apply(directed_graph::vertex_vec_t &in_solution) override;
	};

	virtual bool apply(directed_graph &g, directed_graph::vertex_vec_t &in_solution, int &parameter,
					   reverse_rules_stack<directed_graph> &reverse_rules);

	virtual std::string description() const override;

private:
	void insertClique(directed_graph &g, const directed_graph::vertex_vec_t &cycleV, int &parameter,
					  reverse_rules_stack<directed_graph> &reverse_rules);

	bool handleSubgraph(directed_graph &g, directed_graph &copy, const directed_graph::vertex_vec_t &componentV,
						int &parameter,
						reverse_rules_stack<directed_graph> &reverse_rules);

	bool tryWithArticulation(directed_graph &g, directed_graph &g_no_2way_edge,
							 directed_graph mySubgraph,
							 int &parameter,
							 reverse_rules_stack<directed_graph> &reverse_rules);
};