#include "dome_rule.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;

/*!
 * Decides whether a is subset of b
 */
bool is_subset_of(directed_graph::vertex_set_t a, directed_graph::vertex_set_t b) {
    if(a.size() > b.size()) return false;
    for(auto& x : a)
        if(b.find(x) == b.end())
            return false;
    return true;
}

bool dome_rule::apply(directed_graph &g)
{
    bool res = false;
    for (vertex_t u: g.get_vertices())
    {
        vertex_set_t non_pi_predecessors;
        for (vertex_t p: g.predecessors(u))
        {
            if (!g.contains_edge(u, p))
            {
                non_pi_predecessors.insert(p);
            }
        }
        auto successors = g.successors(u);
        for (vertex_t v: successors)
        {
            if (g.contains_edge(v, u))
                continue;
            if (is_subset_of(non_pi_predecessors,g.predecessors(v)))
            {
                g.remove_edge(u, v);
                res = true;
                continue;
            }
            vertex_set_t non_pi_successors;
            for (vertex_t s: g.successors(v))
            {
                if (!g.contains_edge(s, v))
                {
                    non_pi_successors.insert(s);
                }
            }
            if (is_subset_of(non_pi_successors,g.successors(u)))
            {
                g.remove_edge(u, v);
                res = true;
                continue;
            }
        }
    }

    return res;
}


string dome_rule::description() const
{
    return "DOME";
}


