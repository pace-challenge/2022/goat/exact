#pragma once

#include "nonsolution_rule.hpp"
#include "../graph/directed_graph.hpp"

#include <vector>
#include <utility>

/*
 * After u, v is removed and added to each component separately, returns edges that are in DAGS only (they are in a DAG component and arent u and v)
 */
std::vector<std::pair<directed_graph::vertex_t, directed_graph::vertex_t>> get_dominated_edges(
                const directed_graph & one_way_subraph, directed_graph::vertex_t u, directed_graph::vertex_t v);

/*
 * Tries to apply get_dominated_edges and remove them
 */
class dominated_cycles_rule: public nonsolution_rule<directed_graph>
{
public:
    virtual bool apply(directed_graph& g) override;

    virtual std::string description() const override;
};

