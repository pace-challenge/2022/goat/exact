#include "flower.hpp"
#include "../graph/utils.hpp"

using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;


bool flower::doWork(directed_graph &g, vertex_vec_t &in_solution, int &parameter, vertex_t v, std::vector<vertex_vec_t> & bucket_q)
{
    if (std::min(g.in_degree(v), g.out_degree(v)) <= static_cast<size_t>(parameter)
        && std::min(g.in_degree(v), g.out_degree(v)) > 0)
    {
        bucket_q[std::min(g.in_degree(v), g.out_degree(v)) - 1].push_back(v);
    } else
    {
        std::pair<bool, size_t> petal_res = petal(g, v, parameter);
        if (!petal_res.first)
        {
            in_solution.push_back(v);
            --parameter;
            g.remove_vertex(v);
            return true;
        } else if (petal_res.second > 0)
        {
            bucket_q[petal_res.second - 1].push_back(v);
        }
    }
    return false;
}

bool flower::apply(directed_graph &g, vertex_vec_t &in_solution, int &parameter)
{
    if (parameter <= 0)
    {
        return false;
    }

    bool applied_rule = false;

    std::vector <vertex_vec_t> bucket_q;
    bucket_q.resize(parameter);

    size_t bucket_count = parameter;
    const vertex_set_t vertices = g.get_vertices();

    for (const vertex_t v: vertices)
    {
        if (parameter == 0)
        {
            break;
        }
        applied_rule = applied_rule || doWork(g,in_solution,parameter,v, bucket_q);
    }
    for (size_t i = 0; i < bucket_q.size() && i > static_cast<size_t>(parameter) && parameter > 0; --i)
    {
        for (vertex_t v: bucket_q[bucket_count - i - 1])
        {
            applied_rule = applied_rule || doWork(g,in_solution,parameter,v, bucket_q);
        }
    }
    return applied_rule;
}

std::string flower::description() const
{
    return "flower";
}
