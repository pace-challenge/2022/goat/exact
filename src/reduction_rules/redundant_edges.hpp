#pragma once
#include "nonsolution_rule.hpp"
#include "../graph/directed_graph.hpp"

/*!
 * Removes one way edges that does not contribute to any directed cycle containing only one way edges.
 */
class redundant_edges_rule : public nonsolution_rule<directed_graph>
{
 public:
	virtual bool apply(directed_graph& g) override;

	virtual std::string description() const override;
};




