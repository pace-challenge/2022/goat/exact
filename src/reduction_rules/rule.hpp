#pragma once

#include <vector>
#include <cmath>
#include "parameter_rule.hpp"


template<typename Graph>
class rule : public parameter_rule<Graph>
{
public:

    /*!
     * Constructor
     * @param g - graph that is kernelized - will be modified by the rule
     * @param in_solution - vertices that are in the solution - rule might add new vertices
     */
    virtual bool apply(Graph &g, typename Graph::vertex_vec_t &in_solution) = 0;

    virtual bool apply(Graph &g, typename Graph::vertex_vec_t &in_solution, int &parameter) override
    {
        int oldSize = in_solution.size();
        bool ret = this->apply(g, in_solution);
        int diff =  (in_solution.size() - oldSize);
        parameter -= diff;
        return ret;
    };
};
