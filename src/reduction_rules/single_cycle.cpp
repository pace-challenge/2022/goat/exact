#include <queue>
#include "single_cycle.hpp"
#include "../graph/utils.hpp" 

using namespace std;
using vertex_t = directed_graph::vertex_t;

bool single_cycle_rule::apply(directed_graph& g){
    size_t old_size = g.get_vertices_count();
    auto vertices = g.get_vertices();
    for(vertex_t v : vertices){
        if(!g.contains_edge(v,v) && petal(g,v,1).first && !petal(g,v,0).first){
            for(vertex_t p : g.predecessors(v)){
                for(vertex_t s : g.successors(v)){
                    g.add_edge(p,s);
                }
            }
            g.remove_vertex(v);
        }
    }
    return old_size != g.get_vertices_count();
}


std::string single_cycle_rule::description() const
{
    return "remove single cycles";
}
