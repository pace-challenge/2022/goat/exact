#pragma once
#include "nonsolution_rule.hpp"
#include "../graph/directed_graph.hpp"

/*!
 * Deletes all vertices, through which goes only one independent cycle
 */
class single_cycle_rule : public nonsolution_rule<directed_graph>
{
 public:
	virtual bool apply(directed_graph& g) override;

	virtual std::string description() const override;
};
