#pragma once

#include "parameter_reverse_rule.hpp"
#include "reverse_rule.hpp"
#include "../graph/directed_graph.hpp"
#include "../fast_hash_table.hpp"
#include <utility>

/**
 * @brief count how many edges should be fixed to create a clique,
 * count pairs of 1way edges, eg. if is missing edge u->v and v->u, then returns 1
 */
int countMissingEdges(const directed_graph &g, const directed_graph::vertex_vec_t &vertices);

class struction : public parameter_reverse_rule<directed_graph>
{
public:
	// map vertex -> pair of vertices
	using newlyAddedMapper = fast_map<directed_graph::vertex_t, std::pair<directed_graph::vertex_t, directed_graph::vertex_t>>;

	class struction_reverse : public reverse_rule<directed_graph>
	{
		directed_graph::vertex_t v_0;
		directed_graph::vertex_vec_t neighbourhood;
		newlyAddedMapper newlyAdded;
		void replaceInSolution(directed_graph::vertex_vec_t &in_solution,
							   const directed_graph::vertex_vec_t &addToSolution) const;

	public:
		struction_reverse(directed_graph::vertex_t v_0, const directed_graph::vertex_vec_t &v_0p, const newlyAddedMapper &map)
			: v_0(v_0), neighbourhood(v_0p), newlyAdded(map) {}

		virtual bool reverse_apply(directed_graph::vertex_vec_t &in_solution) override;
	};

	virtual bool apply(directed_graph &g, directed_graph::vertex_vec_t &in_solution, int &parameter,
					   reverse_rules_stack<directed_graph> &reverse_rules);

	virtual std::string description() const override;

	bool dangerous_apply(directed_graph &g, directed_graph::vertex_vec_t &in_solution, int &parameter,
						 reverse_rules_stack<directed_graph> &reverse_rules,
						 int &danger_param);

private:
	bool canApplyOn(const directed_graph &g, const directed_graph::vertex_t &v_0, const int &danger_param) const;

	directed_graph::vertex_vec_t getOrdering(const directed_graph &g, directed_graph::vertex_t v_0);

	void addVertices(directed_graph &g, const directed_graph::vertex_vec_t &neighbourhood,
					 newlyAddedMapper &map, directed_graph::vertex_vec_t &newlyAdded);
	void addEdge(directed_graph &g, const newlyAddedMapper &map, const directed_graph::vertex_vec_t &newlyAdded);

	void replaceWithNewVertices(directed_graph &g, const directed_graph::vertex_t &v_0, const directed_graph::vertex_vec_t &neighbourhood,
								const newlyAddedMapper &map, const directed_graph::vertex_vec_t &newlyAdded);

	void augmentGraph(directed_graph &g, const directed_graph::vertex_t &v_0,
					  int &parameter, reverse_rules_stack<directed_graph> &reverse_rules,
					  int &danger_param);
};