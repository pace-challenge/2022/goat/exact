#pragma once

#include "rule.hpp"
#include "../graph/directed_graph.hpp"

/*!
 * 
 */
class subgraph_subset_neigborhood : public rule<directed_graph>
{
 public:
	virtual bool apply(directed_graph& g, directed_graph::vertex_vec_t & in_solution ) override;

	virtual std::string description() const override;
};
