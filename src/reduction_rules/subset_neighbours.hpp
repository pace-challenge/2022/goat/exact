#pragma once
#include "rule.hpp"
#include "../graph/directed_graph.hpp"

/*!
 * Reduction rule that reduces instance where there is two vertices joined by two way edge and ones neighbours are subset of the other vertex neighbours.
 */
class subset_neighbours_rule : public rule<directed_graph>
{
 public:
	virtual bool apply(directed_graph& g, directed_graph::vertex_vec_t & in_solution ) override;

	virtual std::string description() const override;
};




