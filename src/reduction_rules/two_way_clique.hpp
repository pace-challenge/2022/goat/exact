#pragma once
#include <string>

#include "rule.hpp"
#include "../graph/directed_graph.hpp"
/**
 * important: need an instance with no self loop, otherwise might give wrong vertices in in_solution
 * run single_cycle or uselessvertexdeletion if needed before using this rule
 * 
 * finds a clique in graph that is of size >= 3 and all edges are two-way(there exist (u, w) and (w, u) for every pair in clique)
 * if there exists a vertex ``v`` that has edges only in its clique, then remove all of its neighbours
 * 		to remove all edges in clique (=> all cycles)
 */
class two_way_clique : public rule<directed_graph>
{
public:
	virtual bool apply(directed_graph & g, directed_graph::vertex_vec_t & in_solution ) override;

	virtual std::string description() const override;
};