#pragma once

#include "nonsolution_rule.hpp"
#include "../graph/directed_graph.hpp"
/*!
 * A vertex v is useless if and only if there are all
 * edges (p,s) for each predecessor p of v and successor s of v.
 * Note that this is trivially true for the sink and source vertices.
 *
 * Deletes all such vertices, complexity is O(t*n*m), where t is number of vertices deleted
 */
class useless_vertex_deletion : public nonsolution_rule<directed_graph>
{
 public:
	virtual bool apply(directed_graph& g) override;
	virtual std::string description() const override;
 private:
	static bool is_useless(const directed_graph& g, directed_graph::vertex_t v);
};




