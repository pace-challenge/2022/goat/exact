#include "vc_style_chord.hpp"

#include "../graph/utils.hpp"
#include "../graph/generics.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;

#include <queue>


vector<pair<vertex_t, vertex_t>> get_noncycle_edges_after_removal(const directed_graph & graph, vertex_vec_t vertices){
    directed_graph copy = graph;
    for(vertex_t v: vertices){
        copy.remove_vertex(v);
    }
    vector<pair<vertex_t, vertex_t>> noncycle_edges = get_non_scc_edges(copy);
    // add edges incident to vertices, since the are currently in no cycle
    for(vertex_t v: vertices){
        for(vertex_t pred : graph.predecessors(v)){
            noncycle_edges.push_back({pred,v});
        }
        for(vertex_t succ : graph.successors(v)){
            noncycle_edges.push_back({v,succ});
        }
    }
    sort(noncycle_edges.begin(), noncycle_edges.end());
    return noncycle_edges;
}

bool vc_style_chord::apply(directed_graph& graph)
{
    bool applied = false;
    queue<directed_graph> q;
    enqueue_oneway_components(q, graph);
    while(!q.empty()){
        directed_graph subgraph = q.front();
        q.pop();
        directed_graph one_way_subraph = subgraph; 
        remove_2cycles_from_graph(one_way_subraph);
        bool component_changed = false;
        for(const vertex_t v : subgraph.get_vertices()){
            vertex_vec_t two_way_neigborhood = get_two_way_neigborhood(subgraph, v);
            if(two_way_neigborhood.size() == 0 ){
                // degree zero vertices won't delete any edge if redundadnt_edges has been run before
                continue;
            }
            if(two_way_neigborhood.size() ==1){
                // if v has degree 1 we rather do the second vertex, but that also can be of degree 1
                if(get_two_way_neigborhood(subgraph, two_way_neigborhood[0]).size() > 1 || v < two_way_neigborhood[0]){
                    continue;
                }
            }
            vector<pair<vertex_t, vertex_t>> noncycle_edges0 = get_noncycle_edges_after_removal(one_way_subraph, {v});
            vector<pair<vertex_t, vertex_t>> noncycle_edges1 = get_noncycle_edges_after_removal(one_way_subraph, two_way_neigborhood);
            vector<pair<vertex_t, vertex_t>> intersection = get_intersection_sorted(noncycle_edges0, noncycle_edges1);
            for(auto [u,v] : intersection){
                graph.remove_edge(u,v);
                subgraph.remove_edge(u,v);
                one_way_subraph.remove_edge(u,v);
            }
            if(!intersection.empty()){
                component_changed = true;
                applied = true;
            }
        }
        if(component_changed){
            enqueue_oneway_components(q, subgraph);
        }
    }
    return applied;
}

std::string vc_style_chord::description() const
{
    return "VC style chord";
}
