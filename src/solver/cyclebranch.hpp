#pragma once
#include "../graph/directed_graph.hpp"
#include "../reduction_rules/reverse_rule.hpp"
#include "abstract_solver.hpp"
#include <vector>
/*
 * Solver that branches on shortest cycles
 */
class cyclebranch_solver : public abstract_solver
{
public:
	typename directed_graph::vertex_vec_t solve_dfvs(const directed_graph &graph) override;
	std::string description() const override;

private:
	enum branch_code
	{
		NO_BRANCHING,
		NO_SOLUTION,
		SOLUTION_FOUND
	};

	/*
	 * Deletes to_delete vertices from the graph, adds them to deleted. Reduces the graph and then does the branching.
	 */
	bool branch_wrapper(const directed_graph &graph, directed_graph::vertex_vec_t &deleted, int k,
						const directed_graph::vertex_vec_t &to_delete);

	bool branch_wrapper(const directed_graph &graph, directed_graph::vertex_vec_t &deleted, int k,
						directed_graph::vertex_t to_delete)
	{
		return branch_wrapper(graph, deleted, k, directed_graph::vertex_vec_t({to_delete}));
	}

	/*
	 * Splits graph into componenets and solves the components by increasing the sol_size one by one on each component
	 */
	bool handle_multiple_components(const directed_graph &graph, const std::vector<directed_graph::vertex_vec_t> &sccs,
									directed_graph::vertex_vec_t &deleted, int k);
	/*
	 * Handles branching.
	 */
	bool chooseBranchStrategy(const directed_graph &g, directed_graph::vertex_vec_t &deleted, int k);

	bool branch_on_best_cycle(const directed_graph &graph, directed_graph::vertex_vec_t &deleted, int k);

	/*
	 * More effective branching algorithm, that branches only on cycles of size 2
	 * if there are no 2 cycles NO_BRANCHING is returned and still DFVS of size k-deleted.size() may exist
	 */
	branch_code branch_on_2_cycles(const directed_graph &graph, directed_graph::vertex_vec_t &deleted, int k);

	branch_code solve_with_UB(const directed_graph &graph, directed_graph::vertex_vec_t &deleted, int k);

	//----------------------------------------------------------------------------------

	int getLB_slow(const directed_graph &graph) const;
	int getLB_fast(const directed_graph &graph) const;
	directed_graph::vertex_vec_t getUB_solution(const directed_graph &graph) const;
	int getUB(const directed_graph &graph) const;

	bool solve_with_parameter(const directed_graph &graph, directed_graph::vertex_vec_t &solution);
	bool solve_with_parameter(const directed_graph &graph, directed_graph::vertex_vec_t &solution, int ub);
	bool solve_with_parameter(const directed_graph &graph, directed_graph::vertex_vec_t &solution, int lb, int ub);

	bool solve_with_parameter_linear(const directed_graph &graph, directed_graph::vertex_vec_t &solution, int lb, int ub);
	bool solve_with_parameter_bsearch(const directed_graph &graph, directed_graph::vertex_vec_t &solution, int lb, int ub, bool print = false);

	bool solve_with_parameter_k(const directed_graph &graph, directed_graph::vertex_vec_t &solution, int k);

	//----------------------------------------------------------------------------------

	void chooseSolveStrategy(const directed_graph &graph, directed_graph::vertex_vec_t &solution);

	//----------------------------------------------------------------------------------

	branch_code solve_with_vertex_cut(const directed_graph &graph, directed_graph::vertex_vec_t &solution);
	branch_code solve_with_vertex_cut(const directed_graph &graph, directed_graph::vertex_vec_t &solution, int k);

	branch_code solve_with_vertex_cut2(const directed_graph &graph, directed_graph::vertex_vec_t &solution,
									   int maxComponentSize, bool parametrized, int ub);

	branch_code solve_with_weak_cut(const directed_graph &graph, directed_graph::vertex_vec_t &solution,
									int maxComponentSize, bool parametrized, int ub);
	branch_code solve_with_weak_cut(const directed_graph &graph, directed_graph::vertex_vec_t &solution,
									int cutSize, int maxComponentSize, bool parametrized, int ub);

	branch_code solve_with_strong_cut(const directed_graph &graph, directed_graph::vertex_vec_t &solution,
									  int maxComponentSize, bool parametrized, int ub);
	branch_code solve_with_strong_cut(const directed_graph &graph, directed_graph::vertex_vec_t &solution,
									  int cutSize, int maxComponentSize, bool parametrized, int ub);

	branch_code solve_with_VertexCut(bool weaklyConnected, const directed_graph &graph, directed_graph::vertex_vec_t &solution,
									 int cutSize, int maxComponentSize, bool parametrized, int ub);

	bool handleOneCut(const directed_graph &graph, directed_graph::vertex_vec_t &solution,
					  const directed_graph::vertex_vec_t &componentV, const directed_graph::vertex_vec_t &vertexCut,
					  directed_graph::vertex_vec_t &toDeleteFromCut,
					  bool parametrized, int &budget);

	bool handleOneCut_2Cut(const directed_graph &graph, directed_graph::vertex_vec_t &solution,
						   const directed_graph::vertex_vec_t &componentV, const directed_graph::vertex_vec_t &vertexCut,
						   directed_graph::vertex_vec_t &toDeleteFromCut,
						   bool parametrized, int &budget, const directed_graph::vertex_vec_t &originalSolution);
	//----------------------------------------------------------------------------------

	branch_code solve_with_VertexCover(const directed_graph &graph, directed_graph::vertex_vec_t &solution);
	branch_code solve_with_VertexCover(const directed_graph &graph, directed_graph::vertex_vec_t &solution, int ub);
	branch_code solve_with_VertexCover(const directed_graph &graph, directed_graph::vertex_vec_t &solution, int lb, int ub);

	//----------------------------------------------------------------------------------

	bool solve_with_SCC(const directed_graph &graph, directed_graph::vertex_vec_t &solution);

	//----------------------------------------------------------------------------------

	// how much k can we afford to increase, this param gets higher the more work has been done
	// eg if reduced k by a lot, we can afford to increase the param a little bit
	int dangerous_param = 0;
	void update_danger_param(int reduced_k);
	branch_code applyDangerousReduction(directed_graph graph, directed_graph::vertex_vec_t &solution);
};